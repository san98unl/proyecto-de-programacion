'use strict';
var rol = require('../modelos/rol');
var persona = require('../modelos/persona');
var cuenta = require('../modelos/cuenta');
var r_m_p = require('../modelos/r_m_p');
/**
 * Controlador que gestiona las funciones del Medico
 */
class MedicoController {
    /**
     * Metodo que llama el fragmento lista_pacientes y manda la lista de los pacientes
     * que estan asociados a un doctor
     * @param {type} req
     * @param {type} res
     * @returns {undefined} llama el fragmento y muestra la lista de pacientesque estan asociados a un doctor
     */
    mostrar_lista_pacientes(req,res){
        r_m_p.getJoin({persona: {rol:true}}).run().then(function (relacion) {
            console.log(relacion);
             res.render('index',
                        {title: 'Pacientes',
                            fragmento: "lista_pacientes",
                            sesion: true,
                          
                            lista: relacion,
                            msg: {error: req.flash('error'), info: req.flash('info')}
                        });
            
        }).error();
    }
    /**
     * Metodo que muestra el formulario para que el medico pueda agregar pacientes
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    para_agregar_paciente(req, res) {
        res.render('index', {title: 'Principal', fragmento: "agregar_paciente", sesion: true, usuario: req.user.nombre,
            dat: {med: req.user.rol}, msg: {error: req.flash('error'), info: req.flash('info')}});
    }
    /**
     * Metodo que permite a un medico buscar los pacientes que tiene registrado por sus apellidos
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    buscar(req, res) {
        var texto = req.query.texto;
        var data = {};

        persona.getJoin({rol: true}).filter({apellidos: texto}).then(function (lista) {
            lista.forEach(function (item, index) {
                    data[index] = {
                        nombres: item.nombres,
                        apellidos: item.apellidos,
                        edad: item.edad,
                        telefono: item.telefono,
                        ident:item.id
                    };
                    });
            res.json(data);
        }).error(function (error) {
            req.flash('error', 'Hibo u8n problema fatal');
            res.redirect('/sitios');
        });
    }
     /**
     * Metodo que permite al medico que ha iniciado sesion registrar a un paciente y relaciaonarlo a el
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    agregar_paciente(req, res) {
        var data = {
            id_med: req.user.id,
            id_paci: req.body.id_paciente

        };
         var relacion = new r_m_p(data);
        relacion.save().then(function (guardado) {
//                    res.send(guardado);
            req.flash('info', 'Se ha guardado correctamente');
            res.redirect('/');
        }).error(function (error) {
            req.flash('error', 'no se ');
            res.redirect('/registro/sitio');
        });
    }
}
module.exports = MedicoController;