'use strict';
var fs = require('fs');
var rol = require('../modelos/rol');
var persona = require('../modelos/persona');
var cuenta = require('../modelos/cuenta');

var persona_modelo = require('../modelos/persona');
var calificacion_modelo = require('../modelos/calificacion_modelo');
var rol_modelo = require('../modelos/rol');
var nivel_modelo = require('../modelos/nivel_modelo');
var pregunta_modelo = require('../modelos/pregunta_modelo');
var respuesta_modelo = require('../modelos/respuesta_modelo');
/**
 * Controloador que controla las funciones de los Niveles
 */
class GestionNivelesController {
    /**
     * Metodo que llama el fragmento "gestion_niveles" 
     * @param {type} req
     * @param {type} res
     * @returns {undefined} la visualizacion del fragmento gestion_niveles
     */
    gestionNivelesMain(req, res) {
        if (req.session !== undefined && req.user !== undefined) {
            nivel_modelo.getJoin({preguntas: {respuestas: true}}).then(niveles => {
                res.render(
                        'gestion_niveles',
                        {
                            code: 0, //Indica que no hubo errores
                            title: 'Gestionar Niveles',
                            sesion: true,
                            lista_niveles: niveles,
                        }
                );
            }).catch(error => {
                res.render(
                        'gestion_niveles',
                        {
                            code: -1, //Indica suceso de error
                            title: 'Gestionar Niveles',
                            sesion: true,
                            lista_niveles: [],
                        }
                );
            });
        } else {
            res.redirect('/');
        }
    }
    /**
     * Metodo que permite registrar un nivel usando metodos recursivos
     * @param {type} req
     * @param {type} res
     * @returns {Object} el registro del nivel
     */
    registrarNivel(req, res) {
        //if(req.session !== undefined && req.user !== undefined) {
        let data = req.body;

        console.log(req.body);
        console.log(req.files);
        if (data.external_id === '0' && data.id_nivel === '0') {
            let nivel = new nivel_modelo({
                id_persona: '',
                nota_maxima: parseInt(data.nota_maxima),
                nota_minima: parseInt(data.nota_minima),
                descripcion: data.descripcion,
                orden: parseInt(data.orden),
                id_categoria: parseInt(data.id_categoria)
            });
            copiarArchivosRecursivoRetornarDataListaPreguntas(req, 0, data, nivel, [], function (preguntas) {
                console.log('volvi de las preguntas');
                console.log(preguntas);
                if (preguntas != null) {
                    nivel.preguntas = preguntas;
                    nivel.saveAll({preguntas: {respuestas: true}}).then(function (nivel_saved) {
                        console.log(nivel_saved);
                        res.json({code: 0, data: 'Nivel registrado'});
                    }).catch(function (error) {
                        console.log(error);
                        res.json({code: -1, data: 'Error al guardar el nivel'});
                    });
                } else {
                    console.log(error);
                    res.json({code: -1, data: 'Error al guardar el nivel'});
                }
            });
        } else {
            nivel_modelo.getJoin({preguntas: {respuestas: true}}).filter({external_id: data.external_id}).then(function (nivel_old) {
                let aux_respuestas = [];
                nivel_old[0].preguntas.forEach(function (p) {
                    p.respuestas.forEach(function (r) {
                        aux_respuestas.push(r);
                    });
                });
                pregunta_modelo.filter({id_nivel: data.id_nivel}).delete().then(function () {
                    console.log('preguntas borradas');
                    deleteRespuesasNivelOld(0, aux_respuestas, function (a) {
                        console.log('respuestas borradas');
                        if (a) {
                            updateNivelServer(data, req, res);
                        } else {
                            res.json({code: -1, data: 'Error al actualizar el nivel a'});
                        }
                    });
                }).catch(function (error) {
                    console.log(error);
                    res.json({code: -1, data: 'Error al actualizar el nivel b'});
                });
            }).catch(function (error) {
                console.log(error);
                res.json({code: -1, data: 'Error al actualizar el nivel c'});
            });
        }
        //}else{
        //    res.json({code:-1,data:'Error, no tienes permisos para realizar esta acción'});
        //}
    }
    /**
     * Metodo que permite eliminar un Nivel
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    eliminarNivel(req, res) {
        if (req.session !== undefined && req.user !== undefined) {
            nivel_modelo.get(req.body.id_nivel).delete().then(function () {
                res.json({code: 0, data: 'Nivel eliminado'});
            }).catch(function (error) {
                res.json({code: -1, data: 'Error al eliminar el nivel'});
            });
        } else {
            res.json({code: -1, data: 'Error, no tienes permisos para realizar esta acción'});
        }
    }
    /**
     * Metodo que muestra las personas con su rol para su posterior modificacion
     * @param {type} req
     * @param {type} res
     * @returns {undefined} objeto personas que sus datos son recorridos y plasmados en una tabla
     */
    editarRolUsuarios(req, res) {
        if (req.session !== undefined && req.user !== undefined) {
            persona_modelo.getJoin({rol: true}).then(function (personas) {
                console.log('aqui');
                console.log(personas);
                rol_modelo.then(function (roles) {
                    console.log(roles);
                    res.render(
                            'gestion_usuarios',
                            {
                                personas: personas,
                                lista_roles: roles
                            }
                    );
                }).catch(function (error) {
                    res.redirect('/');
                });
            }).catch(function (error) {
                res.redirect('/');
            });
        } else {
            res.redirect('/');
        }
    }
    /**
     * Metodo que actualiza el rol de un Usuario 
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    updateRolUsuario(req, res) {
        rol_modelo.get(req.body.id_rol).then(function (roles) {
            if (req.session !== undefined && req.user !== undefined) {
                 if (roles.nombre !== "Administrador" ) {
                persona_modelo.get(req.body.id_persona).update({id_rol: req.body.id_rol}).then(function () {
                    res.json({code: 0, data: 'Rol actualizado'});
                    console.log("tiṕop "+roles.nombre);
                }).catch(function (error) {
                    console.log(error);
                    res.json({code: -1, data: 'Error no se pudo actualizar el rol'});
                });
            } 
        }else {
                res.redirect('/');
            }
        });

    }

    listarCalificaciones(req, res) {
        persona_modelo.getJoin({rol: true}).then(function (pacientes) {
            nivel_modelo.then(function (niveles) {
                calificacion_modelo.getJoin({nivel: true, persona: true}).then(function (calificaciones) {
                    console.log(calificaciones);
                    res.render(
                            'listar_calificaciones',
                            {
                                title: 'Listar Calificaciones',
                                lista_pacientes: pacientes,
                                lista_niveles: niveles,
                                lista_calificaciones: calificaciones
                            }
                    );
                }).catch(function (error) {
                    res.redirect('/');
                });
            }).catch(function (error) {
                res.redirect('/');
            });
        }).catch(function (error) {
            res.redirect('/');
        });
    }

}
module.exports = GestionNivelesController;

/**
 * Metodo que de manera recursiva permite generar dinamicamente las preguntas y respuestas,
 * Genera restricciones para subir archivo 
 * Recorre la lista de preguntas conjuntamente con sus respuestas y copia los archivos multimedia al servidor 
 *  y retorna una lista de objetos del modelo pregunta que es ligada luego al nivel
 * @param {type} req
 * @param {int} pos posicion o numero de pregunta
 * @param {Array} data datos obtenidos del formulario para crearpreguntas
 * @param {Object} nivel nivel 
 * @param {Array} preguntas lista los datos de las preguntas obtenidas del formulario
 * @param {type} callback
 * @returns {undefined}
 */
function copiarArchivosRecursivoRetornarDataListaPreguntas(req, pos, data, nivel, preguntas, callback) {
    if (pos < JSON.parse(data.lista_preguntas).length) {
        let p = JSON.parse(data.lista_preguntas)[pos];
        let pregunta = new pregunta_modelo({
            pregunta: p.pregunta,
            tipo_archivo: parseInt(p.tipo_archivo),
            url_archivo: '',
        });
        pregunta.nivel = nivel;
        if (p.tipo_archivo !== 3) {
            let file = getFileOfData(req, p.file_name);
            if (file != null) {
                coyFile(file.path, 'archivos/' + file.originalname, function (a) {
                    if (a) {
                        pregunta.url_archivo = 'archivos/' + file.originalname;
                        //Copiar los archivos de las respuestas
                        copiarArchivoRespuestasToServer(req, 0, pregunta, p.respuestas, [], function (b) {
                            if (b !== null) {
                                pregunta.respuestas = b;
                                preguntas.push(pregunta);
                                pos = pos + 1;
                                copiarArchivosRecursivoRetornarDataListaPreguntas(req, pos, data, nivel, preguntas, callback);
                            } else {
                                callback(null);
                            }
                        });
                    } else {
                        callback(null);
                    }
                });
            } else {
                callback(null);
            }
        } else {
            copiarArchivoRespuestasToServer(req, 0, pregunta, p.respuestas, [], function (b) {
                if (b !== null) {
                    pregunta.respuestas = b;
                    preguntas.push(pregunta);
                    pos = pos + 1;
                    copiarArchivosRecursivoRetornarDataListaPreguntas(req, pos, data, nivel, preguntas, callback);
                } else {
                    callback(null);
                }
            });
        }
    } else {
        callback(preguntas);
    }
}
/**
 * * Metodo que de manera recursiva permite generar dinamicamente las respuestas,
 * Genera restricciones para subir archivo 
 * Recorre la lista de respuestas y copia los archivos multimedia al servidor 
 *  y retorna una lista de objetos del modelo de respuestas 
 * @param {type} req
 * @param {type} pos numero de la respuesta
 * @param {type} pregunta objeto pregunta con el que se va a relacionar
 * @param {type} lista_respuestas lista obtenida de los datos del formulario para ingresar las respuestas
 * @param {type} respuestas arreglo en el que se almacenaran las respuestas
 * @param {type} callback
 * @returns {undefined}
 */
function copiarArchivoRespuestasToServer(req, pos, pregunta, lista_respuestas, respuestas, callback) {
    if (pos < lista_respuestas.length) {
        let r = lista_respuestas[pos];
        let respuesta = new respuesta_modelo({
            respuesta: r.respuesta,
            tipo_archivo: parseInt(r.tipo_archivo),
            respuesta_correcta: parseInt(r.respuesta_correcta),
            url_archivo: '',
        });
        respuesta.pregunta = pregunta;
        if (r.tipo_archivo !== 3) {
            let file = getFileOfData(req, r.file_name);
            if (file !== null) {
                coyFile(file.path, 'archivos/' + file.originalname, function (a) {
                    if (a) {
                        respuesta.url_archivo = 'archivos/' + file.originalname;
                        //Copiar los archivos de las respuestas
                        respuestas.push(respuesta);
                        pos = pos + 1;
                        copiarArchivoRespuestasToServer(req, pos, pregunta, lista_respuestas, respuestas, callback);
                    } else {
                        callback(null);
                    }
                });
            } else {
                callback(null);
            }
        } else {
            respuestas.push(respuesta);
            pos = pos + 1;
            copiarArchivoRespuestasToServer(req, pos, pregunta, lista_respuestas, respuestas, callback);
        }
    } else {
        callback(respuestas);
    }
}
/**
 * Metodo que permite obtener el Archivo acorde a la pregunta
 * @param {type} req
 * @param {type} file_name
 * @returns {getFileOfData.file_return|file}
 */
function getFileOfData(req, file_name) {
    let file_return = null;
    req.files.forEach(function (file) {
        console.log(file);
        console.log(file_name);
        if (file.fieldname === file_name) {
            file_return = file;
        }
    });
    console.log(file_return);
    return file_return;
}
/**
 * Metodo que copia los archivos a la carpeta del servidor  
 * @param {type} tmp_path
 * @param {type} target_path
 * @param {type} callback
 * @returns {undefined}
 */
function coyFile(tmp_path, target_path, callback) {
    var src = fs.createReadStream(tmp_path);
    var dest = fs.createWriteStream(target_path);
    src.pipe(dest);
    src.on('end', function () {
        callback(true);
    });
    src.on('error', function (err) {
        console.log(err);
        callback(false);
    });
}

/**
 * Borra toda las respuestas del Nivel
 * antes de modificar
 * @param {type} pos
 * @param {type} respuestas
 * @param {type} callback
 * @returns {undefined}
 */
function deleteRespuesasNivelOld(pos, respuestas, callback) {
    if (pos < respuestas.length) {
        respuesta_modelo.get(respuestas[pos].id).delete().then(function () {
            pos = pos + 1;
            deleteRespuesasNivelOld(pos, respuestas, callback);
        }).catch(function (error) {
            console.log(error);
            callback(false);
        });
    } else {
        callback(true);
    }
}
/**
 * Permite actualizar un Nivel
 * @param {type} data
 * @param {type} req
 * @param {type} res
 * @returns {undefined}
 */
function updateNivelServer(data, req, res) {
    //Update nivel
    console.log(data);
    let nivel = new nivel_modelo({
        id: data.id_nivel,
        external_id: data.external_id,
        id_persona: '',
        nota_maxima: parseInt(data.nota_maxima),
        nota_minima: parseInt(data.nota_minima),
        descripcion: data.descripcion,
        orden: parseInt(data.orden),
        id_categoria: parseInt(data.id_categoria)
    });
    copiarArchivosRecursivoRetornarDataListaPreguntasInUpdateNivel(req, 0, data, nivel, [], function (preguntas) {
        console.log('volvi de las preguntas');
        console.log(preguntas);
        if (preguntas != null) {
            nivel.preguntas = preguntas;
            nivel.setSaved();
            nivel.saveAll({preguntas: {respuestas: true}}).then(function (nivel_saved) {
                console.log(nivel_saved);
                res.json({code: 0, data: 'Nivel actualizado'});
            }).catch(function (error) {
                console.log(error);
                res.json({code: -1, data: 'Error al actualizar el nivel'});
            });
        } else {
            res.json({code: -1, data: 'Error al actualizar el nivel'});
        }
    });

}
/**
 *  Metodo que de manera recursiva permite obtener los datos el nivel y genera  dinamicamente las preguntas y respuestas,
 * para su posterior modificacion
 * Recorre la lista de preguntas conjuntamente con sus respuestas y copia los archivos multimedia al servidor 
 *  y retorna una lista de objetos del modelo pregunta que es ligada luego al nivel. Y genera una validacion para copiar 
 *  archivos nuevos o mantener los anteriores en cada pregunta y respuesta
 *  
 * @param {type} req
 * @param {type} pos
 * @param {type} data
 * @param {type} nivel
 * @param {type} preguntas
 * @param {type} callback
 * @returns {undefined}
 */
function copiarArchivosRecursivoRetornarDataListaPreguntasInUpdateNivel(req, pos, data, nivel, preguntas, callback) {
    if (pos < JSON.parse(data.lista_preguntas).length) {
        let p = JSON.parse(data.lista_preguntas)[pos];
        let pregunta = new pregunta_modelo({
            pregunta: p.pregunta,
            tipo_archivo: parseInt(p.tipo_archivo),
            url_archivo: (typeof (p.url_archivo_old) !== 'undefined') ? p.url_archivo_old : '',
        });
        console.log('modelo p');
        console.log(pregunta);
        pregunta.nivel = nivel;
        if (pregunta.url_archivo.length <= 0) {
            if (p.tipo_archivo !== 3) {
                let file = getFileOfData(req, p.file_name);
                console.log('file geted');
                console.log(file);
                if (file != null) {
                    coyFile(file.path, 'archivos/' + file.originalname, function (a) {
                        if (a) {
                            pregunta.url_archivo = 'archivos/' + file.originalname;
                            //Copiar los archivos de las respuestas
                            copiarArchivoRespuestasToServerInUpdateNivel(req, 0, pregunta, p.respuestas, [], function (b) {
                                if (b !== null) {
                                    pregunta.respuestas = b;
                                    preguntas.push(pregunta);
                                    pos = pos + 1;
                                    copiarArchivosRecursivoRetornarDataListaPreguntas(req, pos, data, nivel, preguntas, callback);
                                } else {
                                    callback(null);
                                }
                            });
                        } else {
                            callback(null);
                        }
                    });
                } else {
                    callback(null);
                }
            } else {
                copiarArchivoRespuestasToServerInUpdateNivel(req, 0, pregunta, p.respuestas, [], function (b) {
                    if (b !== null) {
                        pregunta.respuestas = b;
                        preguntas.push(pregunta);
                        pos = pos + 1;
                        copiarArchivosRecursivoRetornarDataListaPreguntas(req, pos, data, nivel, preguntas, callback);
                    } else {
                        callback(null);
                    }
                });
            }
        } else {
            copiarArchivoRespuestasToServerInUpdateNivel(req, 0, pregunta, p.respuestas, [], function (b) {
                if (b !== null) {
                    pregunta.respuestas = b;
                    preguntas.push(pregunta);
                    pos = pos + 1;
                    copiarArchivosRecursivoRetornarDataListaPreguntas(req, pos, data, nivel, preguntas, callback);
                } else {
                    callback(null);
                }
            });
        }
    } else {
        callback(preguntas);
    }
}
/**
 * Metodo que de manera recursiva permite obtener los datos el nivel y genera  dinamicamente las respuestas,
 * para su posterior modificacion
 * Recorre la lista de respuestas y copia los archivos multimedia al servidor 
 * 
 * @param {type} req
 * @param {type} pos
 * @param {type} pregunta
 * @param {type} lista_respuestas
 * @param {type} respuestas
 * @param {type} callback
 * @returns {undefined}
 */
function copiarArchivoRespuestasToServerInUpdateNivel(req, pos, pregunta, lista_respuestas, respuestas, callback) {
    if (pos < lista_respuestas.length) {
        let r = lista_respuestas[pos];
        console.log('data desde e js');
        console.log(r);
        let respuesta = new respuesta_modelo({
            respuesta: r.respuesta,
            tipo_archivo: parseInt(r.tipo_archivo),
            respuesta_correcta: parseInt(r.respuesta_correcta),
            url_archivo: (typeof (r.url_archivo_old) !== 'undefined') ? r.url_archivo_old : '',
        });
        console.log('en el metodo');
        console.log(respuesta);
        if (respuesta.url_archivo.length <= 0) {
            respuesta.pregunta = pregunta;
            if (r.tipo_archivo !== 3) {
                let file = getFileOfData(req, r.file_name);
                if (file !== null) {
                    coyFile(file.path, 'archivos/' + file.originalname, function (a) {
                        if (a) {
                            respuesta.url_archivo = 'archivos/' + file.originalname;
                            //Copiar los archivos de las respuestas
                            respuestas.push(respuesta);
                            pos = pos + 1;
                            copiarArchivoRespuestasToServerInUpdateNivel(req, pos, pregunta, lista_respuestas, respuestas, callback);
                        } else {
                            callback(null);
                        }
                    });
                } else {
                    callback(null);
                }
            } else {
                respuestas.push(respuesta);
                pos = pos + 1;
                copiarArchivoRespuestasToServerInUpdateNivel(req, pos, pregunta, lista_respuestas, respuestas, callback);
            }
        } else {
            respuestas.push(respuesta);
            pos = pos + 1;
            copiarArchivoRespuestasToServerInUpdateNivel(req, pos, pregunta, lista_respuestas, respuestas, callback);
        }
    } else {
        callback(respuestas);
    }
}
