'use strict';
var rol = require('../modelos/rol');
var persona = require('../modelos/persona');
var cuenta = require('../modelos/cuenta');
/**
 * Controlador que gestiona las funciones de Persona
 */
class PersonaController {
     /**
     * Metodo que le permite a un usuario registrarse
     * con sus datos personales y crear una cuenta para su posterior inicio de sesion
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
    guardar_persona_cuenta(req, res) {
        rol.filter({nombre: "Paciente"}).run().then(function (roles) {
            if (roles.length > 0) {
                //para validar que el correo no este repetido
                cuenta.filter({username: req.body.username}).run().then(function (cuentaB) {
                    if (cuentaB == 0) {
                        //guardo
                        var role = roles[0];
                        var dataP = {
                            apellidos: req.body.apellidos,
                            nombres: req.body.nombres,
                            telefono: req.body.telefono,
                            edad: req.body.edad,
                            id_rol: role.id
                        };
                        var personaP = new persona(dataP);
                        var dataC = {
                            username: req.body.username,
                            clave: req.body.clave
                        };
                        var cuentaP = new cuenta(dataC);
                        personaP.cuenta = cuentaP;
                        personaP.saveAll({cuenta: true}).then(function (personaSave) {
//                            res.send(personaSave);
                            req.flash('info', 'Se ha guardado correctamente');
                            res.redirect('/');
                        }).error(function (error) {

                            req.flash('error', 'Hibo u8n problema fatal');
                            res.redirect('/registro');
                        });
                    } else {
                        req.flash('error', 'Tu correo ya se ha registrado');
                        res.redirect('/');
                        console.log("regreso");
                    }
                }).error(function (error) {
                    req.flash('error', 'Hibo u8n problema fatal');
                    res.redirect('/registro');
                });

            }
        }).error(function (error) {
            req.flash('error', 'NO hay roles registrado');
            res.redirect('/registro');
        });
    }
//    iniciar_sesion(req, res) {
//
//        cuenta.getJoin({persona: {rol: true}}).filter({correo: req.body.correo}).run().then(function (account) {
//            if (account.length > 0) {
//                var accountA = account[0];
//                if (accountA.clave === req.body.clave) {
//                    //realiza funciones
////                    res.send(accountA);
//                    console.log(accountA);
////        res.render('index', {titulo: 'Inicio de Ssesion', sesion: req.session.usuario, andrea: "fragmentos/frm_principal"});
//
//                } else {
////                    req.flash('error', 'Sus credenciales no son validas');
////                    res.redirect('/login');
//                }
//            } else {
////                req.flash('error', 'Sus credenciales no son validas');
////                res.redirect('/login');
//            }
//        }).error(function (error) {
//
//        });
//
//    }
}
module.exports = PersonaController;

