'use strict';
/**
 * Controlador que reaiza funciones de la Cuenta
 */
class CuentaController{
    /**
    
     * Metodo que destruye la session
      * @kind function
     * @param {type} req
     * @param {type} res
     * @returns {undefined} destruccion de la session
     */
  cerrar_sesion(req,res){
      req.session.destroy();
        res.redirect('/');

  }  
}
module.exports=CuentaController;


