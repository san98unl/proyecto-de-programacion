'use strict';
 var rol = require('../modelos/rol');
    var persona=require('../modelos/persona');
    var cuenta=require('../modelos/cuenta');
 
class AdminController{
   mostrar_lista_personas(req,res){
        persona.getJoin({rol: true}).run().then(function (relacion) {
            console.log(relacion);
             res.render('index',
                        {title: 'Pacientes',
                            fragmento: "listar_personas",
                            sesion: true,
                          
                            lista: relacion,
                            msg: {error: req.flash('error'), info: req.flash('info')}
                        });
            
        }).error();
    }
}
module.exports=AdminController;