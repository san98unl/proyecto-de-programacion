'use strict';
var fs = require('fs');
var rol = require('../modelos/rol');
var persona = require('../modelos/persona');
var cuenta = require('../modelos/cuenta');

var persona_modelo = require('../modelos/persona');
var calificacion_modelo = require('../modelos/calificacion_modelo');
var rol_modelo = require('../modelos/rol');
var nivel_modelo = require('../modelos/nivel_modelo');
var pregunta_modelo = require('../modelos/pregunta_modelo');
var respuesta_modelo = require('../modelos/respuesta_modelo');
/**
 * Controlador que permite la interaccion paciente y examen
 */
class GestionPacientesController {
/**
 * Metodo que permite que el paciente pueda observar los niveles e inetractuar con ellos
 * @param {type} req
 * @param {type} res
 * @returns {undefined}
 */
    pacienteExamenMain(req,res){
        if(req.user){
            console.log('aqui esta el usuario');
            console.log(req.user);
            nivel_modelo.getJoin({preguntas: {respuestas: true}}).orderBy('orden').then(niveles => {
                console.log(niveles);
                calificacion_modelo.getJoin({nivel: true}).filter({id_persona: req.user.id}).orderBy('id_nivel').then(calificaciones => {
                    res.render(
                        'gestion_pacientes',
                        {
                            title: 'Examen Paciente',
                            user: req.user,
                            lista_niveles: niveles,
                            lista_calificaciones: calificaciones,
                        }
                    );
                }).catch(error => {
                    console.log(error);
                    res.redirect('/');    
                });
            }).catch(error => {
                console.log(error);
                res.redirect('/');
            });
        }else{
            res.redirect('/');
        }
    }
    /**
     * Permite que guardar la califacion o encaso q exista modificarla
     * @param {type} req
     * @param {type} res
     * @returns {undefined}
     */
        /**
     * Metodo que llama el fragmento lista_pacientes y manda la lista de los pacientes
     * que estan asociados a un doctor
     * @param {type} req
     * @param {type} res
     * @returns {undefined} llama el fragmento y muestra la lista de pacientesque estan asociados a un doctor
     */
    mostrar_notas(req,res){
        calificacion_modelo.getJoin({nivel:{persona: {rol:true}}}).run().then(function (relacion) {
            console.log(relacion);
            
             res.render('index',
                        {title: 'notas',
                            fragmento: "lista_pacientes",
                            sesion: true,
                          
                            lista: relacion,
                            msg: {error: req.flash('error'), info: req.flash('info')}
                        });
            
        }).error();
    }
    registrarOActualizarCalificacion(req,res){
        if(req.user){
            calificacion_modelo.filter({id_persona: req.body.id_persona, id_nivel: req.body.id_nivel}).then(calificacion_old => {
                console.log(calificacion_old);
                if(calificacion_old.length > 0) {
                    calificacion_old[0].calificacion = req.body.calificacion;
                    calificacion_old[0].setSaved();
                    calificacion_old[0].saveAll({}).then(aux => {
                        res.json({code: 0, data: 'Calificación registrada'});
                    }).catch(error => {
                        res.json({code: -1, data: 'Error al registrar la calificación'});
                    });
                }else{
                    let aux = new calificacion_modelo({
                        id_persona: req.body.id_persona,
                        id_nivel: req.body.id_nivel,
                        calificacion: req.body.calificacion,
                    });
                    aux.saveAll({}).then(aux => {
                        res.json({code: 0, data: 'Calificación registrada'});
                    }).catch(error => {
                        console.log(error);
                        res.json({code: -1, data: 'Error al registrar la calificación'});
                    });
                }
            }).catch(function(error){
                console.log(error);
                res.json({code: -1, data: 'Error al registrar la calificación'});
            });
        }else{
            res.json({code: -1, data: 'Error, no tienes permiso para realizar esta acción'});
        }
    }

}
module.exports = GestionPacientesController;
