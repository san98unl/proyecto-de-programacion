'use strict';
/**
 * Metodo que crea automaticamente los roles de la aplicacion si es que no existe ningun rol
 * Debe ser llamada en el index para que se creeen cada vez que se ejecuta la aplicacion
 * @returns {undefined} la creacion de los roles de Medico, Paciente y Administrador
 */
function crear_roles() {
    var rol = require('../modelos/rol');
    rol.run().then(function (roles) {
        if (roles.length == 0) {
            rol.save([{nombre: "Medico"}, {nombre: "Paciente"}, {nombre: "Administrador"}]);
        }
        console.log(roles);
        console.log(roles.length);
    }).error(function (error) {
        console.log(error);
    });
}
/**
 * Metodo que que crea a la persona Administrador
 * @returns {undefined} un  objeto una persona con el rol de Adminitrador
 */
function crear_admin() {
    var rol = require('../modelos/rol');
    var persona = require('../modelos/persona');
    var cuenta = require('../modelos/cuenta');
    persona.filter({apellidos: "Administrador"}).run().then(function (personas) {
        if (personas.length == 0) {


            rol.filter({nombre: "Administrador"}).run().then(function (roles) {

                var role = roles[0];
                var dataP = {
                    apellidos: "Administrador",
                    nombres: "Administrador",
                    telefono: "0992019148",
                    edad: "21",
                    id_rol: role.id
                };
                var personaP = new persona(dataP);
                var dataC = {
                    username: "admin",
                    clave: "admin"
                };
                var cuentaP = new cuenta(dataC);
                personaP.cuenta = cuentaP;
                personaP.saveAll({cuenta: true}).then(function (personaSave) {
                    console.log(personaSave);
//                            res.send(personaSave);
                    console.log("se ha creado administrador");
                }).error(function (error) {
                    console.log("error no se ha creado administrador");

                });

                console.log(roles);
                console.log(roles.length);
            }).error(function (error) {
                console.log(error);
            });
        }
        ;
    }).error(function (error) {
        console.log(error);

    });

}



module.exports = {crear_roles, crear_admin};

