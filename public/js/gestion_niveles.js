$(document).ready(function(){
    //Inicializa los Tabs
    $('.tabs').tabs();
    //Inicializa los modales
    $('.modal').modal();
    //Inicializa los select de cada formulario
    $('select').formSelect();

    $('#tipo_archivo_respuesta').change(function(){
        let value = parseInt($(this).val());
        if( value === 3) {
            $('#row_FileRespuesta').addClass('hide');
        } else {
            $('#row_FileRespuesta').removeClass('hide');
        }
    });

    $('body').on('click', '.eliminarRespuesta', function(){
        let pos = $(this).attr('data-target');
        $('#respuesta_at_'+pos).remove();

        for(let i=0; i<lista_respuestas_nivel.length; i++) {
            if(lista_respuestas_nivel[i].pos === parseInt(pos)){
                lista_respuestas_nivel.splice(i, 1);
            }
        }
    });

    //Detecta el click en el boton para añadir una respuesta
    $('#btn_add_Respuesta').click(function(){
        let respuesta = $('#respuesta').val();
        let tipo_archivo = parseInt($('#tipo_archivo_respuesta').val());
        let file = $('#respuesta_file')[0].files[0];

        if(tipo_archivo >= 0) {
            if(((tipo_archivo >= 0 || tipo_archivo <= 2) && $('#respuesta_file')[0].files[0]) || (tipo_archivo === 3)){
                let res = {
                    pos: (lista_respuestas_nivel.length > 0) ? lista_respuestas_nivel.length : 0,
                    respuesta: respuesta,
                    tipo_archivo: tipo_archivo,
                    file: (tipo_archivo === 3) ? null : file,
                    respuesta_correcta: $('#respuesta_status').val()
                };
                console.log(res);
                lista_respuestas_nivel.push(res);
                let html  = '<tr id="respuesta_at_'+res.pos+'">';
                    html += '   <td>'+respuesta+'</td>';
                    html += '   <td>'+$('#tipo_archivo_respuesta option:selected').text()+'</td>';
                    if(res.respuesta_correcta === '1'){
                        html += '   <td>Incorrecta</td>';
                    }else{
                        html += '   <td>Correcta</td>';
                    }
                    html += '   <td><a class="eliminarRespuesta" data-target="'+res.pos+'">Eliminar</a></td>';
                    html += '</tr>';
                $('#body_addRespuesta').append(html);

                $('#respuesta').val('');
                $('#tipo_archivo_respuesta').val('-1');
                $('#tipo_archivo_respuesta').formSelect();
                $('#row_FileRespuesta').addClass('hide');
                $('#respuesta_status').val('1');
                $('#respuesta_status').formSelect();
            } else {
                M.toast({html: 'Error, existen campos incompletos'});    
            }
        }else{
            M.toast({html: 'Error, existen campos incompletos'});
        }
    });

    $('#tipo_archivo_pregunta').change(function(){
        let value = parseInt($(this).val());
        if( value === 3 || value === -1) {
            $('#row_FilePregunta').addClass('hide');
        } else {
            $('#row_FilePregunta').removeClass('hide');
        }
    });

    $('body').on('click', '.eliminarPregunta', function(){
        let pos = $(this).attr('data-target');
        $('#pregunta_at_'+pos).remove();

        for(let i=0; i<lista_preguntas_nivel.length; i++) {
            if(lista_preguntas_nivel[i].pos === parseInt(pos)){
                lista_preguntas_nivel.splice(i, 1);
            }
        }
    });

    //Detecta el click en el boton para añadir una pregunta
    $('#btn_add_Pregunta').click(function(){
        let pregunta = $('#pregunta').val();
        let tipo_archivo = parseInt($('#tipo_archivo_pregunta').val());
        let file = $('#pregunta_file')[0].files[0];

        if(pregunta.length > 0 && tipo_archivo >= 0 && lista_respuestas_nivel.length > 0) {
            if(((tipo_archivo >= 0 || tipo_archivo <= 2) && $('#pregunta_file')[0].files[0]) || (tipo_archivo === 3)){
                let pre = {
                    pos: (lista_preguntas_nivel.length > 0) ? lista_preguntas_nivel.length : 0,
                    pregunta: pregunta,
                    tipo_archivo: tipo_archivo,
                    file: (tipo_archivo === 3) ? null : file,
                    respuestas: lista_respuestas_nivel
                };
                console.log(pre);
                lista_preguntas_nivel.push(pre);
                lista_respuestas_nivel = [];
                let html  = '<tr id="pregunta_at_'+pre.pos+'">';
                    html += '   <td>'+pregunta+'</td>';
                    html += '   <td>'+$('#tipo_archivo_pregunta option:selected').text()+'</td>';
                    html += '   <td><a class="eliminarPregunta" data-target="'+pre.pos+'">Eliminar</a></td>';
                    html += '</tr>';
                $('#body_addPregunta').append(html);

                $('#pregunta').val('');
                $('#tipo_archivo_pregunta').val('-1');
                $('#tipo_archivo_pregunta').formSelect();
                $('#row_FilePregunta').addClass('hide');
                $('#body_addRespuesta').find('tr').remove();
            } else {
                M.toast({html: 'Error, existen campos incompletos'});    
            }
        }else{
            M.toast({html: 'Error, existen campos incompletos'});
        }
    });

    $('#guardar_nivel').click(function(){
        let formdata = new FormData();
        if(lista_preguntas_nivel.length > 0 && $('#descripcion').val().length > 0 && parseInt($('#orden').val()) != -1 && parseInt($('#nota_maxima').val()) > 0 && parseInt($('#nota_minima').val()) > 0 && parseInt($('#id_categoria').val()) != -1) {
            let preguntas = [];
            lista_preguntas_nivel.forEach(function(p, i){
                console.log(p);
                let aux = {
                    pregunta: p.pregunta,
                    tipo_archivo: p.tipo_archivo,
                    file_name: '',
                    url_archivo_old: '',
                    respuestas: []
                };
                if(p.tipo_archivo !== 3) {
                    console.log(typeof(p.url_archivo_old));
                    if(typeof(p.url_archivo_old) === 'undefined'){
                        console.log('entre al if');
                        aux.file_name = 'pregunta_file_'+i;
                        formdata.append('pregunta_file_'+i, p.file);
                        p.respuestas.forEach(function(r, j){
                            console.log(r);
                            let res = {
                                respuesta: r.respuesta,
                                tipo_archivo: r.tipo_archivo,
                                respuesta_correcta: r.respuesta_correcta,
                                file_name: ''
                            };
                            if(r.tipo_archivo !== 3) {
                                res.file_name = 'pregunta_respuesta_file_'+i+'_'+j;
                                formdata.append('pregunta_respuesta_file_'+i+'_'+j, r.file);
                            }
                            aux.respuestas.push(res);
                        });
                        console.log('preguntada tratada');
                        console.log(aux);
                    }else{
                        aux.url_archivo_old = p.url_archivo_old;
                        p.respuestas.forEach(function(r, j){
                            let res = {
                                respuesta: r.respuesta,
                                tipo_archivo: r.tipo_archivo,
                                respuesta_correcta: r.respuesta_correcta,
                                url_archivo_old: '',
                                file_name: ''
                            };
                            if(r.tipo_archivo != 3) {
                                res.url_archivo_old = r.url_archivo_old;
                            }
                            aux.respuestas.push(res);
                        });
                    }
                } else{
                    p.respuestas.forEach(function(r, j){
                        console.log(r);
                        let res = {
                            respuesta: r.respuesta,
                            tipo_archivo: r.tipo_archivo,
                            respuesta_correcta: r.respuesta_correcta,
                            url_archivo_old: '',
                            file_name: ''
                        };
                        if(r.tipo_archivo !== 3) {
                            if(typeof(r.url_archivo_old) !== 'undefined'){
                                res.url_archivo_old = r.url_archivo_old;
                            }else{
                                res.file_name = 'pregunta_respuesta_file_'+i+'_'+j;
                                formdata.append('pregunta_respuesta_file_'+i+'_'+j, r.file);
                            }
                        }
                        aux.respuestas.push(res);
                    });
                    console.log('preguntada de tipo texto tratada');
                    console.log(aux);
                }
                preguntas.push(aux);
            });
            console.log(lista_preguntas_nivel);

            formdata.append('external_id', $('#external_id').val());
            formdata.append('id_nivel', $('#id_nivel').val());
            formdata.append('descripcion', $('#descripcion').val());
            formdata.append('orden', $('#orden').val());
            formdata.append('nota_maxima', $('#nota_maxima').val());
            formdata.append('nota_minima', $('#nota_minima').val());
            formdata.append('id_categoria', $('#id_categoria').val());
            formdata.append('lista_preguntas', JSON.stringify(preguntas));
            
            $.ajax({
                url: '/med/registrarNivel',
                type:"POST",
                data: formdata,
                contentType: false,
                processData: false,
            }).done(function (data) {
                console.log(data);
                M.toast({html: data.data});
                if (data.code == 0) {
                    location.reload();
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                M.toast({html: 'Error, no se pudo completar la operación'});
            });

        }else{
            M.toast({html: 'Error, existen campos incompletos'});
        }
    });

    $('#button_menuTopAddNivel').click(function(){
        $('#external_id').val('0');
        $('#id_nivel').val('0');
        $('#descripcion').val('');
        $('#orden').val('');
        $('#nota_maxima').val('');
        $('#nota_minima').val('');
        $('#id_categoria').val('-1');
        $('#id_categoria').formSelect();
        $('#tipo_archivo_respuesta').val('-1');
        $('#tipo_archivo_respuesta').formSelect();
        $('#tipo_archivo_pregunta').val('-1');
        $('#tipo_archivo_pregunta').formSelect();
        $('#body_addPregunta').find('tr').remove();
        $('#body_addRespuesta').find('tr').remove();
        lista_preguntas_nivel = [];
        lista_respuestas_nivel = [];
        if(!$('#row_FilePregunta').hasClass('hide'))
            $('#row_FilePregunta').addClass('hide');
        if(!$('#row_FileRespuesta').hasClass('hide'))
            $('#row_FileRespuesta').addClass('hide');
    });
});