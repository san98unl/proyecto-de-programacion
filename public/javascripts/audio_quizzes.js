$(document).ready(function () {
    console.log("entro");
    play_1_A();
    play_1_B();
    play_1_C();
    play_1_D();
    play_1_E();
   
});
function play_1_A() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/quiz_deteccion/silbar.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#playA').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}
function play_1_B() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/quiz_deteccion/ladrido.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#playB').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}
function play_1_C() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/quiz_deteccion/llamada.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#playC').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}
function play_1_D() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/quiz_deteccion/timbre.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#playD').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}
function play_1_E() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/quiz_deteccion/viento.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#playE').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}


