$(document).ready(function () {
    console.log("entro");
    play_1();
    play_2();
    play_3();
    play_4();
    play_5();
    play_6();
    play_7();
    play_8();
    play_9();
});
function play_1() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/acople_guitarra.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#play').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}
function play_2() {
    var audioElement2 = document.createElement('audio');
    audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement2.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement2.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement2.duration + " seconds");
        $("#source").text("Source:" + audioElement2.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement2.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement2.currentTime);
    });


    $('#play2').click(function () {
        audioElement2.play();
        $("#status").text("Status: Playing");
    });

    $('#pause').click(function () {
        audioElement2.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement2.currentTime = 0;
    });
}
function play_3() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/ambulancia.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#play3').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}
function play_4() {
    var audioElement3 = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement3.setAttribute('src', '/sounds/cascada.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement3.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement3.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement3.duration + " seconds");
        $("#source").text("Source:" + audioElement3.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement3.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement3.currentTime);
    });

    $('#play4').click(function () {
        audioElement3.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement3.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement3.currentTime = 0;
    });
}
function play_5() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/tormenta.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#play5').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}
function play_6() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/conversar_bebes.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#play6').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}
function play_7() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/lavar_ropa.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#play7').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}
function play_8() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/lluvia.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#play8').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}
function play_9() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/piano_violin.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#play9').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}
function play_1() {
    var audioElement = document.createElement('audio');
//     var audioElement2 = document.createElement('audio');
    audioElement.setAttribute('src', '/sounds/acople_guitarra.mp3');
//     audioElement2.setAttribute('src', '/sounds/piar_de_pajaro.mp3');

    audioElement.addEventListener('ended', function () {
        this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
        $("#length").text("Duration:" + audioElement.duration + " seconds");
        $("#source").text("Source:" + audioElement.src);
        $("#status").text("Status: Ready to play").css("color", "green");
    });

    audioElement.addEventListener("timeupdate", function () {
        $("#currentTime").text("Current second:" + audioElement.currentTime);
    });

    $('#play').click(function () {
        audioElement.play();
        $("#status").text("Status: Playing");
    });
//    $('#play2').click(function() {
//        audioElement2.play();
//        $("#status").text("Status: Playing");
//    });

    $('#pause').click(function () {
        audioElement.pause();
        $("#status").text("Status: Paused");
    });

    $('#restart').click(function () {
        audioElement.currentTime = 0;
    });
}