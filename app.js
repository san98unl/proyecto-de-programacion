var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var session=require('express-session');
var flash =require('connect-flash');
var passport = require('passport');

var indexRouter = require('./routes/index');
var paciRouter = require('./routes/paciente');
var medRouter = require('./routes/medico');
var adminRouter = require('./routes/admin');
//var adminNivelesRouter = require('./routes/niveles_routes');
//var pacienteExamenRouter = require('./routes/paciente_routes');
var hbs = require('hbs'); 
hbs.registerPartials(__dirname + '/views/fragmentos'); 

//var hbs = require('hbs');
//var fs = require('fs');
//
//var partialsDir = __dirname + '/../views/fragmentos';
//
//var filenames = fs.readdirSync(partialsDir);

var app = express();

//filenames.forEach(function (filename) {
//  var matches = /^([^.]+).hbs$/.exec(filename);
//  if (!matches) {
//    return;
//  }
//  var name = matches[1];
//  var template = fs.readFileSync(partialsDir + '/' + filename, 'utf8');
//  hbs.registerPartial(name, template);
//});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

//Registro de helpers para HBS, to JSON y Comparar
var hbs = require('hbs');

hbs.registerHelper('json',function(obj) {
	return new hbs.SafeString(JSON.stringify(obj))
})

hbs.registerHelper('comparar', function (v1, operator, v2, options) {

    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '!=':
            return (v1 != v2) ? options.fn(this) : options.inverse(this);
        case '!==':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});

var session = require('express-session');
var flash = require('connect-flash');

app.use(session({
    secret: 'proyecto',
    resave: false,
    saveUninitialized: true
}));
app.use(flash());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/archivos', express.static('archivos'));

app.use(flash());
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
require('./config/passport')(passport);
app.use('/', indexRouter);
app.use('/paci', paciRouter);
app.use('/med', medRouter);
app.use('/admin', adminRouter);
//app.use('/admin/niveles/', adminNivelesRouter);
//app.use('/paciente/examenes', pacienteExamenRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
