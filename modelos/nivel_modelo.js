var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Nivel = thinky.createModel("Nivel", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),
    id_persona: type.string(),
    nota_maxima: type.number(),
    nota_minima: type.number(),
    descripcion: type.string(),
    orden: type.number(),
    id_categoria: type.number(), //0 => Discriminación, 1 => Identificación, 2 => Detección
});
module.exports = Nivel;

var Persona = require('./persona');
Nivel.belongsTo(Persona, "persona","id_persona","id");

var Pregunta = require('./pregunta_modelo');
Nivel.hasMany(Pregunta, 'preguntas', 'id', 'id_nivel');