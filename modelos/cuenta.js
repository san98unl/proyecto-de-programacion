var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var cuenta = thinky.createModel("cuenta", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),
    username: type.string(),
    clave: type.string(),
    createdAt: type.date().default(r.now()),
    updatedAt: type.date().default(r.now()),
    id_persona: type.string()
});
module.exports = cuenta;
var persona = require('./persona');
cuenta.belongsTo(persona, "persona", "id_persona", "id");