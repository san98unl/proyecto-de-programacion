var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var persona = thinky.createModel("persona", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),
    apellidos: type.string(),
    nombres: type.string(),
    edad: type.number(),
    telefono: type.number(),
    id_rol: type.string(),
    createdAt: type.date().default(r.now())
});
module.exports = persona;
var cuenta = require('./cuenta');
persona.hasOne(cuenta, "cuenta", "id", "id_persona");
var rol = require('./rol');
persona.belongsTo(rol, "rol", "id_rol", "id");
var nivel=require('./nivel_modelo');
persona.hasMany(nivel,"nivel","id","id_persona");
var r_m_p=require('./r_m_p');
persona.hasMany(r_m_p,"r_m_p","id","id_paci");

var Calificacion = require('./calificacion_modelo');
persona.hasMany(Calificacion, 'calificaciones', 'id', 'id_persona');