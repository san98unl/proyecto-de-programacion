var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Pregunta = thinky.createModel("Pregunta", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),
    id_nivel: type.string(),
    //Datos de la pregunta
    pregunta: type.string(),
    tipo_archivo: type.number(), // 0 => audio, 1 => imagen, 2 => video
    url_archivo: type.string(),
});

module.exports = Pregunta;

var Nivel = require('./nivel_modelo');
Pregunta.belongsTo(Nivel, 'nivel', 'id_nivel', 'id');

var Respuesta = require('./respuesta_modelo');
Pregunta.hasMany(Respuesta, 'respuestas', 'id', 'id_pregunta');