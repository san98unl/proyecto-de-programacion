var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Calificacion = thinky.createModel("Calificacion", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),
    id_persona: type.string(),
    id_nivel: type.string(),
    calificacion: type.number(),
});

module.exports = Calificacion;

var Persona = require('./persona');
Calificacion.belongsTo(Persona,"persona","id_persona","id");

var Nivel = require('./nivel_modelo');
Calificacion.belongsTo(Nivel,"nivel","id_nivel","id");