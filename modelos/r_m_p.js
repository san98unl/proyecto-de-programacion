var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var r_m_p = thinky.createModel("r_m_p", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),

    id_med: type.string(),
    id_paci: type.string()
});
module.exports = r_m_p;
var persona=require('./persona');
r_m_p.belongsTo(persona,"persona","id_paci","id");