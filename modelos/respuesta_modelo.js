var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Respuesta = thinky.createModel("Respuesta", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),
    id_pregunta: type.string(),
    //Datos de la respuesta
    respuesta: type.string(),
    tipo_archivo: type.number(), // 0 => audio, 1 => imagen, 2=> video, 3 => texto
    url_archivo: type.string(),
    respuesta_correcta: type.number(), //0 correcta, 1 incorrecta
});

module.exports = Respuesta;

var Pregunta = require('./pregunta_modelo');
Respuesta.belongsTo(Pregunta, 'pregunta', 'id_pregunta', 'id');