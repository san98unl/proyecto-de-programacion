//var bCrypt = require('bcrypt-nodejs'); //modulo para encriptar claves


var rol = require('../modelos/rol');
var persona = require('../modelos/persona');
var cuenta = require('../modelos/cuenta');
module.exports = function (passport) {
    var Cuenta = cuenta;//modelo
    var Persona = persona;//modelo
    var LocalStrategy = require('passport-local').Strategy;
    //Permite serializar los datos de cuenta
    passport.serializeUser(function (cuenta, done) {
        done(null, cuenta.id);
    });
    // Permite deserialize la cuenta de usuario
    passport.deserializeUser(function (id, done) {
        
        cuenta.getJoin({persona: {rol: true}}).filter({id: id}).run().then(function (cuenta) {
           console.log(cuenta[0]);
            if (cuenta[0]) {
                var userinfo = {
                    id: cuenta[0].persona.id,
                    nombre: cuenta[0].persona.apellidos + " " + cuenta[0].persona.nombres,
//                    lastname: cuenta[0].persona.apellidos,
//                    name:cuenta[0].persona.nombres,
//                    age:cuenta[0].persona.edad,
                    rol:cuenta[0].persona.rol.nombre
                };
                console.log(userinfo);
                done(null, userinfo);
            } else {
                done(cuenta.errors, null);
            }
        });
    });
    //inicio de sesion
    passport.use('local-signin', new LocalStrategy(
            {
                usernameField: 'username',
                passwordField: 'clave',
                passReqToCallback: true // allows us to pass back the entire request to the callback
            },
            function (req, email, password, done) {


                cuenta.getJoin({persona: {rol: true}}).filter({username: email}).run().then(function (cuentaU) {
                    console.log(cuentaU[0]);
                    var cuenta = cuentaU[0];
                    if (!cuenta) {
                        return done(null, false, {message: req.flash('error', 'Cuenta no existe')});
                    }
                    console.log("elizabeth");
                    console.log(password + "  " + cuenta.clave);
                    if (cuenta.clave !== password) {
                        return done(null, false, {message: req.flash('error', 'Clave incorrecta')});
                    }
                    console.log("***********");
                    var userinfo = cuenta;

                    console.log(userinfo);
                    console.log("***********");
                    return done(null, userinfo);
                }).catch(function (err) {
                    console.log("Error:", err);
                    return done(null, false, {message: req.flash('error', 'Cuenta erronea')});
                });
            }
    ));
};
