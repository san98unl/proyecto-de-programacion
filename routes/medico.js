var express = require('express');
var passport = require('passport');
var evento = require('../controladores/Eventos');
var router = express.Router();

//var audio = require('../modelos/audio');
var cuenta = require('../modelos/cuenta');
//var imagen = require('../modelos/imagen');
//var nivel = require('../modelos/nivel');
var persona = require('../modelos/persona');
var rol = require('../modelos/rol');

var multer  = require('multer');
var upload = multer({ dest: 'tmp_uploads/' });

var personaController = require('../controladores/PersonaController');
var personaC = new personaController();

var cuentaController = require('../controladores/CuentaController');
var cuentaC = new cuentaController();

var medicoController=require('../controladores/MedicoController');
var medicoC=new medicoController();

var gestion_niveles_controller = require('../controladores/GestionNiveles');
var gestion_niveles = new gestion_niveles_controller();


function validarSesion(req) {
return (req.session !== undefined && req.user !== undefined);
}
var auth_med = function (req, res, next) {
    if (!validarSesion(req)) {
        req.flash('error', 'No tiene acceso a estas funciones');
        res.redirect('/');
    } else {
        next();
    }
};
/* GET users listing. */

router.get('/agregar_paciente',auth_med,medicoC.para_agregar_paciente);
router.post('/agregar_paciente',auth_med,medicoC.agregar_paciente);
router.get('/buscar',auth_med,medicoC.buscar);
router.get('/lista/pacientes',auth_med,medicoC.mostrar_lista_pacientes);

router.get('/',auth_med, function (req, res, next) {
    res.render('index', {title: 'Principal', fragmento: "principal", sesion: true, usuario: req.user.nombre,
        dat: {med: req.user.rol}, msg: {error: req.flash('error'), info: req.flash('info')}});
});
//Muestra el archivo hbs donde se listan todos los niveles organizados por categoria
router.get('/gestionNivelesMain',auth_med, gestion_niveles.gestionNivelesMain);
//Registra el nivel en la BD
router.post('/registrarNivel',auth_med, upload.any(), gestion_niveles.registrarNivel);
//Eliminar nivel de la BD
router.post('/eliminarNivel',auth_med, gestion_niveles.eliminarNivel);

router.get('/listarCalificaciones',auth_med, gestion_niveles.listarCalificaciones);

//Cambiar usuario rol


//router.get('/galeria',auth_med, function (req, res, next) {
//    res.render('index', {title: 'CEAL', sesion: true, fragmento: 'galeria', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/principal',auth_med, function (req, res, next) {
//    res.render('index', {title: 'CEAL', sesion: true, fragmento: 'principal', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/deteccion',auth_med, function (req, res, next) {
//    res.render('index', {title: 'Quiz para Deteccion', sesion: true, fragmento: 'quiz', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/discriminacion',auth_med, function (req, res, next) {
//    res.render('index', {title: 'Quiz para Discriminacion', sesion: true, fragmento: 'quiz_discriminacion', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/identificacion',auth_med, function (req, res, next) {
//    res.render('index', {title: 'Quiz para Identificacion', sesion: true, fragmento: 'quiz_identificacion', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/deteccion1',auth_med, function (req, res, next) {
//    res.render('index', {title: 'Quiz para Identificacion', sesion: true, fragmento: 'deteccion_nivel3'});
//});
module.exports = router;
