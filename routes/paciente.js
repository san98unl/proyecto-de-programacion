var express = require('express');
var passport = require('passport');
var evento = require('../controladores/Eventos');
var router = express.Router();

//var audio = require('../modelos/audio');
var cuenta = require('../modelos/cuenta');
//var imagen = require('../modelos/imagen');
//var nivel = require('../modelos/nivel');
var persona = require('../modelos/persona');
var rol = require('../modelos/rol');

var personaController = require('../controladores/PersonaController');
var personaC = new personaController();

var gestion_pacientes_controller = require('../controladores/GestionPacientes');
var gestion_pacientes = new gestion_pacientes_controller();

var cuentaController = require('../controladores/CuentaController');
var cuentaC = new cuentaController();

function validarSesion(req) {
  return (req.session !== undefined && req.user !== undefined);
}
var auth_paci = function (req, res, next) {
    if (!validarSesion(req)) {
        req.flash('error', 'No tiene acceso a estas funciones');
        res.redirect('/');
    } else {
        next();
    }
};

/* GET users listing. */
router.get('/', auth_paci,function (req, res, next) {
    res.render('index', {title: 'Principal', fragmento: "principal", sesion: true, usuario: req.user.nombre,
        dat: {paci: req.user.rol}, msg: {error: req.flash('error'), info: req.flash('info')}});
});
//router.get('/galeria', auth_paci,function (req, res, next) {
//    res.render('index', {title: 'CEAL', sesion: true, fragmento: 'galeria', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/principal',auth_paci, function (req, res, next) {
//    res.render('index', {title: 'CEAL', sesion: true, fragmento: 'principal', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/deteccion',auth_paci, function (req, res, next) {
//    res.render('index', {title: 'Quiz para Deteccion', sesion: true, fragmento: 'quiz', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/discriminacion',auth_paci, function (req, res, next) {
//    res.render('index', {title: 'Quiz para Discriminacion', sesion: true, fragmento: 'quiz_discriminacion', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/identificacion',auth_paci, function (req, res, next) {
//    res.render('index', {title: 'Quiz para Identificacion', sesion: true, fragmento: 'quiz_identificacion', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/deteccion1',auth_paci, function (req, res, next) {
//    res.render('index', {title: 'Quiz para Identificacion', sesion: true, fragmento: 'deteccion_nivel3'});
//});
//
////
//router.get('/quiz/discriminacion3', function (req, res, next) {
//    res.render('index', {title: 'Quiz para Identificacion', sesion: true, fragmento: 'discriminacionnivel3'});
//});
//router.get('/quiz/deteccion3', function (req, res, next) {
//    res.render('index', {title: 'Quiz para Identificacion', sesion: true, fragmento: 'deteccionnivel3', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/identificacion3', function (req, res, next) {
//    res.render('index', {title: 'Quiz para Identificacion', sesion: true, fragmento: 'identificacion', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//
//router.get('/examen1', function (req, res, next) {
//    res.render('index', {title: 'Examen', sesion: true, fragmento: 'examen_1'});
//});
//router.get('/examen2', function (req, res, next) {
//    res.render('index', {title: 'Examen', sesion: true, fragmento: 'examen_2'});
//});
//router.get('/examen3', function (req, res, next) {
//    res.render('index', {title: 'Examen', sesion: true, fragmento: 'examen_3'});
//});
//router.get('/prueba', function (req, res, next) {
//    res.render('index', {title: 'Examen', sesion: true, fragmento: 'quiz_prueba'});
//});
router.get('/notas',gestion_pacientes.mostrar_notas);
router.get('/pacienteExamenMain', auth_paci,gestion_pacientes.pacienteExamenMain);

router.post('/registrarOActualizarCalificacion',auth_paci, gestion_pacientes.registrarOActualizarCalificacion);

module.exports = router;
