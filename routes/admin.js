var express = require('express');
var router = express.Router();
var pacienteController = require('../controladores/PacienteController');
var pacienteC= new pacienteController();

var adminController = require('../controladores/AdminController');
var adminC= new adminController();

var gestion_niveles_controller = require('../controladores/GestionNiveles');
var gestion_niveles = new gestion_niveles_controller();
function validarSesion(req) {
 return (req.session !== undefined && req.user !== undefined);
}
var auth_admin = function (req, res, next) {
    if (!validarSesion(req)) {
        req.flash('error', 'No tiene acceso a estas funciones');
        res.redirect('/');
    } else {
        next();
    }
};
/* GET users listing. */
router.get('/',auth_admin, function (req, res, next) {
    res.render('index', {title: 'Principal', fragmento: "principal", sesion: true, usuario: req.user.nombre,
        dat: {admin: req.user.rol}, msg: {error: req.flash('error'), info: req.flash('info')}});
});
router.get('/listas',auth_admin,adminC.mostrar_lista_personas);

router.get('/editarRolUsuarios',auth_admin, gestion_niveles.editarRolUsuarios);

router.post('/updateRolUsuario', auth_admin,gestion_niveles.updateRolUsuario);

module.exports = router;

