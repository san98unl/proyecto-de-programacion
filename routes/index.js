var express = require('express');
var passport = require('passport');
var evento = require('../controladores/Eventos');
var router = express.Router();

//var audio = require('../modelos/audio');
var cuenta = require('../modelos/cuenta');
//var imagen = require('../modelos/imagen');
//var nivel = require('../modelos/nivel');
var persona = require('../modelos/persona');
var rol = require('../modelos/rol');

var personaController = require('../controladores/PersonaController');
var personaC = new personaController();

var cuentaController = require('../controladores/CuentaController');
var cuentaC = new cuentaController();

var medicoController = require('../controladores/MedicoController');
var medicoC = new medicoController();

/* GET home page. */
router.get('/', function (req, res, next) {
    if (req.session !== undefined && req.user !== undefined) {
        if (req.user.rol == 'Medico') {
            res.redirect("/med");
            console.log(req.user.nombre);
        } else if (req.user.rol == 'Paciente') {
            res.redirect("/paci");
            console.log(req.user.nombre);
        }else if (req.user.rol == 'Administrador') {
            res.redirect("/admin");
            console.log(req.user.nombre);
        }
//        res.render('index', {title: 'Principal', fragmento: "principal", sesion: true, usuario: req.user.nombre, dat:{nombre_rol:req.user.rol},msg: {error: req.flash('error'), info: req.flash('info')}});
    } else {
        evento.crear_roles();
        evento.crear_admin();

        res.render('index', {title: 'CEAL', sesion: false, msg: {error: req.flash('error'), info: req.flash('info')}});
    }


});

//router.get('/galeria', function (req, res, next) {
//    res.render('index', {title: 'CEAL', sesion: true, fragmento: 'galeria', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/principal', function (req, res, next) {
//    res.render('index', {title: 'CEAL', sesion: true, fragmento: 'principal', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/deteccion', function (req, res, next) {
//    res.render('index', {title: 'Quiz para Deteccion', sesion: true, fragmento: 'quiz', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/discriminacion', function (req, res, next) {
//    res.render('index', {title: 'Quiz para Discriminacion', sesion: true, fragmento: 'quiz_discriminacion', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/identificacion', function (req, res, next) {
//    res.render('index', {title: 'Quiz para Identificacion', sesion: true, fragmento: 'quiz_identificacion', msg: {error: req.flash('error'), info: req.flash('info')}});
//});


//router.get('/galeria', function (req, res, next) {
//    res.render('index', {title: 'CEAL', sesion: true, fragmento: 'galeria', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/principal', function (req, res, next) {
//    res.render('index', {title: 'CEAL', sesion: true, fragmento: 'principal', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/deteccion', function (req, res, next) {
//    res.render('index', {title: 'Quiz para Deteccion', sesion: true, fragmento: 'quiz', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/discriminacion', function (req, res, next) {
//    res.render('index', {title: 'Quiz para Discriminacion', sesion: true, fragmento: 'quiz_discriminacion', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/identificacion', function (req, res, next) {
//    res.render('index', {title: 'Quiz para Identificacion', sesion: true, fragmento: 'quiz_identificacion', msg: {error: req.flash('error'), info: req.flash('info')}});
//});
//router.get('/quiz/deteccion1', function (req, res, next) {
//    res.render('index', {title: 'Quiz para Identificacion', sesion: true, fragmento: 'deteccion_nivel3'});

router.post('/registro/persona', personaC.guardar_persona_cuenta);
router.post('/inicio_sesion',
        passport.authenticate('local-signin',
                {successRedirect: '/',
                    failureRedirect: '/',
                    failureFlash: true}
        ));
router.get('/cerrar_sesion', cuentaC.cerrar_sesion);
module.exports = router;
